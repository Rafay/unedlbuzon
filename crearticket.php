<?php 

    require 'conf/conf_backend.php';
    require 'conf/conf_frontend.php';

    $nombre = strtoupper( $_POST['nombre'] );
    $matricula = strtoupper( $_POST['matricula'] );
    $programa_edu = intval($_POST['programa_educativo']);
    $area_uni = intval($_POST['area_universidad']);
    $correo = $_POST['correo'];
    $mensaje = $_POST['mensaje'];
    $ticket_tipo = $_POST['ticket_tipo'];

    $validation_params = true;
    if( !preg_match('/[A-Za-zÁÉÍÓÚáéíóúÜüñÑ]{3,}(\s+[A-Za-z]{3,}){0,4}/', $nombre) ) {
        $validation_params = false;
    }  
    
    if( !preg_match('/[A-Z\d]{5,15}/', $matricula) ) {
        $validation_params = false;
    }

    if( $programa_edu  < 1 || $programa_edu > count( $appclient['licenciaturas'] ) ) {
        $validation_params = false;
    }

    if( $area_uni  < 1 || $area_uni > count( $appclient['universidad_areas'] ) ) {
        $validation_params = false;
    }

    if( !preg_match('/[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ\d\.,:;¿\?!¡_\-\(\)\[\]\s\'"]{10,500}/', $mensaje) ) {
        $validation_params = false;
    }    

    if( !preg_match('/(QUEJA|SUGERENCIA)/',$ticket_tipo) ) {
        $validation_params = false;
    }

    if( isset($correo) && !preg_match('/[\w\.-]+@[a-zA-Z\-]+(\.[a-z]{2,3}){1,3}/', $correo) ) {
        $validation_params = false;
    }

    if( !$validation_params ) {
        header("Location: ./index.html?error=ERROR_REQUEST");
        return;
    }

    // api configuracion
    // iniciar sesiòn
    $curl = curl_init();
    if( $curl == false ) {
        header('Location: ./index.html?error=ERROR_CONNECTION');
        return;
    }

    curl_setopt($curl, CURLOPT_URL, $buzon['mantis_url']);

    // ajustar el método de la petición
    curl_setopt($curl, CURLOPT_POST, true);

    // ajustar que la respuesta se regrese como string
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($curl, CURLOPT_HEADER, false);

    // ajustar las cabeceras de la petición
    $headers = array(
        'Content-Type:application/json'
        ,"Authorization:{$buzon['authorization_token']}" );

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);    

    // agregar los custom_fields que se agregan al proyecto
    $custom_fields = array( array( 'field' => array('name' => 'Nombre') , 'value' => $nombre )
        ,array( 'field' => array('name' => 'Matricula')     , 'value' => $matricula )
        ,array( 'field' => array('name' => 'ProgramaEducativo') , 'value' => $appclient['licenciaturas'][$programa_edu] )
        ,array( 'field' => array('name' => 'Area')          , 'value' => $appclient['universidad_areas'][$area_uni] )
        ,array( 'field' => array('name' => 'Correo')        , 'value' => $correo )        
    );

    // generar el 'payload' de la petición
    $request = array(        
         'category' => 0
        ,'summary'  => (strcmp($ticket_tipo,"QUEJA") == 0 ? "Queja" : "Sugerencia" ) . " para " . $appclient['licenciaturas'][$programa_edu]
        ,'description'  => $mensaje
        ,'project'  => $buzon['project']
        ,'custom_fields'  => $custom_fields );    

    // ajustar el 'payload' a la petición
    curl_setopt($curl, CURLOPT_POSTFIELDS,  json_encode($request)  );

    //llamada de API
    $response = curl_exec($curl);

    // verificar que el ticket si fue creado 
    if( $response != false)    
        $ticket = json_decode($response, true);
	else
		$ticket = array('error' => true
					   ,'message' => 'Error en la comunicación con MantisBT');

    if( !isset( $ticket['issue'] ) ) { // si no existe el $ticket entonces regresar con mensaje a la página anterior
        $error = true;
        $titulo = $ticket_tipo;  
        include './quejasugerencias_view.php';            
    }
    else {
        //redireccionar a página indexbzn
        header("Location: ./index.html?ticket_registrado={$ticket_tipo}");
    }

    //cerrar sesión
    curl_close($curl);

?>
