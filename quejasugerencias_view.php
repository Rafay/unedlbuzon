<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <?php 
        require 'conf/conf_frontend.php';

        $licenciaturas = $appclient['licenciaturas'];
        $areas_uni = $appclient['universidad_areas'];

        echo "<title>{$titulo}</title>";
    ?>

    <script src="js/sweetalert.min.js"></script>

    <script>

    function validarCampos(event) {

        var validacion_msj = new String();

        var nombre = document.getElementById("nombre").value;
        var matricula = document.getElementById("matricula").value;
        var program_edu = Number.parseInt(document.getElementById("programa_educativo").value);
        var area_uni = Number.parseInt(document.getElementById("area_universidad").value);
        var mensaje = document.getElementById("mensaje").value;
        var correo = document.getElementById("correo").value;

        if( !nombre.match(/[A-Za-zÁÉÍÓÚáéíóúÜüñÑ]{3,}(\s+[A-Za-z]{3,}){0,4}/) ) {
            validacion_msj = 'El campo "Nombre" debe ser válido. Sólo letras y espacios.';            
        }
        else if( !matricula.match(/[A-Za-z\d]{5,15}/) ) {
            validacion_msj = 'El campo "Matrícula" debe ser válido (hasta 15 caracteres).';
        }
        else if( program_edu <= 0 || program_edu >= <?php echo count($licenciaturas); ?>  ) {
            validacion_msj = 'El campo "Programa Educativo" es obligatorio.'
        }
        else if( area_uni <= 0 || area_uni >= <?php echo count($areas_uni); ?>  ) {
            validacion_msj = 'El campo "Áreas de la Universidad" es obligatorio.'
        }
        else if( !mensaje.match(/[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ\d\.,:;¿\?!¡_\-\(\)\[\]\s'"]{10,500}/) ) {
            validacion_msj = 'El campo "Mensaje" es obligatorio y debe tener entre 10 y 500 caracteres.'
        }
        else if( !correo.match(/[\w\.-]+@[a-zA-Z\-]+(\.[a-z]{2,3}){1,3}/) ) {
            validacion_msj = 'El campo "Correo" debe ser válido.'
        }

        console.log("Mensaje: "+ validacion_msj);

        if( validacion_msj.length > 0 ) {
            swal("Error", validacion_msj );
            return false;
        }

        return true;
    }


    function error_message(event) {
        var nombre = document.getElementById("nombre").value;

        if( nombre.length > 0 ) {
            swal("Error","Hubo un problema al crear el ticket. Si este persiste, favor de intentar más tarde." );
        }
    }


    function  regresar(){
        window.location = "index.html";
    }

    </script>

    <style>
        * {
            box-sizing: border-box;
        }

        input[type=text], select, textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;

        }

        body {
            background-color: #1C2652;
            background-size: cover;
            background-repeat: no-repeat;
            font-family: Arial, sans-serif;
            font-weight: bold;
            font-size: 15px;
        }



        label {
            padding: 12px 12px 12px 0;
            display: inline-block;
            font-family:"Arial";
            font-style: italic;
            font-weight: 700;
            color: #1c2652;
        }

        input[type=button], input[type=submit] {
            background-color: #946913;
            color: white;
            padding: 12px 20px;
            border-style :15px;
            border-radius: 10px;
            cursor: pointer;
            float: right;

        }

        input[type=button]:hover, input[type=submit]:hover {
            background-color: #a1a1a1;

        }

        .container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }

        .col-25 {
            float: left;
            width: 25%;
            margin-top: 6px;
        }

        .col-75 {
            float: left;
            width: 75%;
            margin-top: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        h1{
            color: #fff;
            font-family:"Arial";
        }

        .piePagina{
            color:white;

        }
        .linea{
            height:10px;
            background-color:#946913;
            border:none;
        }
        .linea2{
            height:35px;
            background-color:  #f6d119 ;
            border:none;
            padding: all;
            margin:-.7% 0;
        }
        p{
            font-family:"Arial";
        }


        /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
        @media (max-width:  600px) {
            .col-25, .col-75, input[type=submit] {
                width: 100%;
                margin-top: 0;
            }
        }
    </style>

</head>
<body bgcolor="#1c2652" onload="error_message(event)">


<form action='crearticket.php' method='post' name='form' onsubmit='return validarCampos(event)' >
    <input type="hidden" name="ticket_tipo" value=<?php echo $titulo; ?>  >
    <div > <img src="css/logo_unedl-1.png" width="250" height="72,9167"></div>
    <hr/ class="linea" >
    <hr/ class="linea2">
    <font  color="white"><h2> <?php echo $titulo; ?> </h2></font>
    <font color ="white"><p class="parrafo">campos con * son obligatorios</p></font>
    <div class="container">
        <div class="row">
            <div class="col-25">
                <label id="lname" >Nombre*</label>
            </div>
            <div class="col-75">
                <?php
                    if ( !$error )
                        echo '<input type="text" id="nombre" name="nombre" placeholder="Nombre Completo" >';
                    else
                        echo "<input type='text' id='nombre' name='nombre' value='{$nombre}' placeholder='Nombre Completo' >";
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label id="lcorreo" >Correo Electrónico*</label>
            </div>
            <div class="col-75">
                <?php
                    if(!$error)
                        echo "<input type='text' id='correo' name='correo' placeholder='Correo Electrónico'>";
                    else
                        echo "<input type='text' id='correo' name='correo' value='{$correo}' placeholder='Correo Electrónico'>";
                ?>                
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label id="lmatricula">Matrícula*</label>
            </div>
            <div class="col-75">
                <?php 
                    if(!$error)
                        echo "<input type='text' id='matricula' name='matricula' placeholder='Matrícula de estudiante'>";
                    else
                        echo "<input type='text' id='matricula' name='matricula' value='{$matricula}' placeholder='Matrícula de estudiante'>";
                ?>
                
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label id="lProgramaEducativo">Programa Educativo*</label>
            </div>
            <div class="col-75">
                <select id="programa_educativo" name="programa_educativo" style="height:40px">
                    <option value="0" >Seleccione una opción</option>
                    <?php                    
                        foreach( array_keys( $licenciaturas ) as $key ) {
                            if( $error && $key == $programa_edu ) 
                                echo "<option id='pe{$key}' value='{$key}' selected >{$licenciaturas[$key]}</option>";                            
                            else 
                                echo "<option id='pe{$key}' value='{$key}'>{$licenciaturas[$key]}</option>";                            
                        }   
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label id="lAreauni">Áreas de la Universidad*</label>
            </div>
            <div class="col-75">
                <select id="area_universidad" name="area_universidad" style="height:40px" >
                    <option value="0" >Seleccione una opción</option>
                    <?php
                        foreach( array_keys($areas_uni) as $key ){
                            if( $error && $key == $area_uni )
                                echo "<option id='au{$key}' value='{$key}' selected >{$areas_uni[$key]}</option>";
                            else
                                echo "<option id='au{$key}' value='{$key}' >{$areas_uni[$key]}</option>";
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label id="subject">Mensaje*</label>
            </div>
            <div class="col-75">
                <?php
                    if( !$error )
                        echo "<textarea id='mensaje' name='mensaje' placeholder='Escribir Mensaje' style='height:200px'></textarea>";
                    else
                        echo "<textarea id='mensaje' name='mensaje' placeholder='Escribir Mensaje' style='height:200px'>{$mensaje}</textarea>";
                ?>
            </div>
        </div>
        <div class="row">
            <input type="submit" value="Enviar">
 	        <input type="button" value="Regresar" onclick="regresar()">
        </div>
    </div>
        <div class='piePagina'>
            <hr/>
            <p>Dudas al siguiente correo: analistadecalidad@unedl.edu.mx </p>
            <span style="float:right"> <a href="https://unedl.edu.mx/portal/datos-personales.php" style="color: #946913;" >Aviso de privacidad - Términos y condiciones</a></span>
		  ©Universidad Enrique Díaz de León 2019, Innsoft, All rights reserverd         
        </div>
</form>

</body>

</html>
